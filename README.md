Se requiere elaborar un pequeño pokédex que permita consultar la información de cualquier pokemon registrado, para ello se adjuntan tres (3) archivos JSON:

- **pokedex.json:** Contiene información básica de cada Pokémon (nombre, id, tipo, etc). 

- **moves.json:** Contiene información sobre cada movimiento y ataque de todos los pokemon.

- **types.json:** Contiene la información de todos los tipos de pokémon disponibles en esta base de datos (fire, water, dragon, etc.).

Esta data la encontraras en la ruta "**src/app/thumbnails**"

Adicionalmente se provee una carpeta “**thumbnails**” en la ruta "**src/assets/thumbnails**" que contiene imágenes de cada pokémon.

En este repositorio encontrarás una aplicación de angular base sin mayores cambios generada a partir del angular@cli

**Concretamente los requerimientos son:**

1. Crear un **fork** o **clonar** este repositorio utilizando **git**
1. Elaborar una pantalla de login que solicite un email y contraseña con validaciones básicas, el campo email debe estar debidamente validado para aceptar solo emails válidos, y el campo contraseña debe requerir mínimo 6 caracteres **alfanuméricos** con al menos uno de ellos siendo un carácter especial (? . @ # [ ] ’ , / + \* -), visualmente deben ofrecer algún tipo de feedback cuando el campo posea errores o no cumpla con las validaciones. Cuando las validaciones sean correctas, al momento de “iniciar sesion” debe redirigir a la pantalla principal (aunque sea una simulación, no debe permitir avanzar si el email y contraseña no están debidamente validados).
1. Elaborar un dashboard (debe ser la ruta principal), esta debe contar con una tabla donde se carguen todos los pokémons con su información básica mostrando los siguientes campos: id, nombre, ataque, defensa, y velocidad. Adicionalmente se requiere la creación de dos campos que permitan filtrar en tiempo real la data de la tabla, estos serian: **Nombre, y Tipo.** Ejemplo: si en el campo **Nombre** fuera ingresado “char”, y en el campo **Tipo** fuera seleccionado “Fire”, debe actualizarse el contenido de la tabla para mostrar solo aquellos pokemons que cumplan con el criterio (a modo de ejemplo,  el resultado de esta búsqueda tendría entre los pokemons resultantes en la tabla a: Charmander, Charmeleon, Charizard). Al hacer doble click sobre algún pokémon en la tabla debe redirigir a una pantalla de detalles.
1. Elaborar una pantalla de detalles para mostrar el pokémon seleccionado en la pantalla principal, allí se deben mostrar los datos del pokémon: Nombre, Tipo, Atributos, Imagen, así como su set de movimientos (esto es dado por el tipo de pokémon, OJO, solo es necesario mostrar el nombre de los movimientos y nada más). En esta misma pantalla debe ser posible editar mediante un pequeño formulario el nombre y tipo del pokemon, asi como sus atributos base, cuando se vuelva a la pantalla principal y se consulte, se debe poder encontrar el pokemon previamente actualizado con los nuevos datos. (evidentemente al reiniciar o refrescar la pagina, se perdera la informacion, solo se requiere ver la actualización sobre la data en tiempo real, no debe alterar los JSON). 
1. Finalmente, al momento de culminar con la prueba por favor háznosla llegar en un enlace en un **repositorio público** al correo electronico: [**hola@quikpago.com**](mailto:hola@quikpago.com)

**Consideraciones:** 

- Debes utilizar GIT para clonar la prueba, y debes igualmente utilizarlo para confirmar tus cambios (commit) y subirlo a un repositorio público al finalizar.
- Puedes utilizar **cualquier framework de css o librería** de componentes para agilizar el proceso (bootstrap, nebular, tailwinds, ng-zorro, clarity design, bulma, etc).
- Tiene prioridad y mayor peso al momento de evaluar la prueba realizar los aspectos funcionales, sin embargo **también se tomará en cuenta** la experiencia de usuario implementada (UI/UX).
- **No está permitido** modificar de ninguna forma los JSON que se adjuntan para resolver la prueba.
  - Se recomienda consumir los JSON a través del HttpClient de angular para simular una petición web, sin embargo puedes consumirlos como mejor te parezca.

